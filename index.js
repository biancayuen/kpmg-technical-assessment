const csv = require('csv-parser');
const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const EventEmitter = require('events');
const fs = require('fs');
const _ = require('lodash');

const INPUT_CONTACTS_FILE_PATH = './inputs/people.csv';
const INPUT_KEYWORDS_FILE_PATH = './inputs/keywords.csv';
const OUTPUT_CONTACTS_FILE_PATH = './outputs/people.csv';

let keywords = [];
const contactCsvContent = [];
let csvWriterConfig = {
    path: OUTPUT_CONTACTS_FILE_PATH
}

/* read and extract the keywords file */
function readKeywords() {
  fs.createReadStream(INPUT_KEYWORDS_FILE_PATH)
    .pipe(csv({
      mapValues: ({header, index, value}) => value.toLowerCase()
    }))
    .on('data', (data) => keywords.push(data))
    .on('end', () => {
      console.log('loaded keywords', keywords.length);
      ee.emit('readContacts');
  });
}

/* read the keywords document */
function readContacts() {
  fs.createReadStream(INPUT_CONTACTS_FILE_PATH)
    .pipe(csv({
      mapHeaders: ({header, index}) => header.replace(/[^a-zA-Z ]|\s/g,'')
    }))
    .on('headers', (headers) => {
      /* csv-writer expects header objects in this id+title object format */
      let csvHeaders = headers.map( header => {
          return { id: header, title: header };
      });
      /* Add any new column headers here
      csvHeaders.push({ id: 'NEW_FIELD_ONE', title: 'NEW_FIELD_ONE' });
      */
      /* update the csv-writer config */
      csvWriterConfig.header = csvHeaders;
    })
    .on('data', (row) => {
      /* add extra content to each row */
      const title = row['Title'];
      if (title) {
        row['Description'] = getLabelsFromTitle(title);
      }
      /* add the row to the output array */
      contactCsvContent.push(row);
    })
    .on('end', () => {
      console.log('loaded contacts', contactCsvContent.length);
      ee.emit('writeContacts');
  });
}

function writeContacts() {
  /* write out the csv */
  createCsvWriter(csvWriterConfig)
    .writeRecords(contactCsvContent)
      .then(() => {
          console.log('finished writing csv', contactCsvContent.length);
          ee.emit('end');
      });
}

function getLabelsFromTitle(title) {
  /*
  / check if title contains the keyword
  / convert the matching tags in csv format to a string of hashtags
  */
  let value = _
    .chain(keywords)
    .filter(tag => _.includes(title.toLowerCase(), tag.keyword.toLowerCase()),0)
    .flatMap(tag => [tag.area, tag.niche, tag.hashtag])
    .uniq()
    .sort()
    .map(tag => '#' + tag)
    .join(' ')
    .value();

  return value;
}

var ee = new EventEmitter()
ee.on('readKeywords', function (text) { readKeywords() });
ee.on('readContacts', function (text) { readContacts() });
ee.on('writeContacts', function (text) { writeContacts() });
ee.emit('readKeywords');
